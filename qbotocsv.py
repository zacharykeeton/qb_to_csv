#! /usr/bin/python
import re

f = open('/home/zachary/Downloads/download.QBO','r')

#patternstring = '((?<=<TRNTYPE>)[^<]+).+((?<=<DTPOSTED>)[^<]+).+((?<=<TRNAMT>)[^<]+).+((?<=<FITID>)[^<]+).+((?<=<NAME>)[^<]+)'

for line in f:
	if line.startswith('<STMTTRN>'):
		#pattern = re.search(patternstring,line)
		
		# did not use long, grouped pattern so as not to rely on 
		# the ordering of the elements
		trntype = re.search('(?<=<TRNTYPE>)[^<]+',line).group(0)
		dtposted = re.search('(?<=<DTPOSTED>)[^<]+',line).group(0)
		trnamt = re.search('(?<=<TRNAMT>)[^<]+',line).group(0)
		fitid = re.search('(?<=<FITID>)[^<]+',line).group(0)
		name = re.search('(?<=<NAME>)[^<]+',line).group(0)
		memo = re.search('(?<=<MEMO>)[^<]+',line)
		
		csvstring = trntype + ',' + dtposted + ',' + trnamt + ',' + fitid + ',' + name
		
		if memo is not None:
			csvstring += ','+memo.group(0)

		print csvstring

